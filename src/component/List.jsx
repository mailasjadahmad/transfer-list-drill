import React, { useState } from "react";

function List({ listArr, onListChange }) {
  const [transferList, setTransferList] = useState([]);

  const [selectedList, setSelectedList] = useState([]);
  function HandleCheckBox(e) {
    // console.log(e.target.checked)
    if (e.target.checked == true) {
      const tempArr=transferList;
      tempArr.push(e.target.value);
      setTransferList(transferList);
      
    } else {
      if (transferList.includes(e.target.value)) {
        const tempArr = transferList;
        let index = tempArr.findIndex((element) => element === e.target.value);
        if (index !== -1) {
          tempArr.splice(index, 1);
          setTransferList(tempArr);
        }
      }
    }

    onListChange(transferList)
    // console.log(e.target.value);
    // setSelectedList([...selectedList,e.target.value])
  }
  return (
    <ul
      class="checklist"
      style={{
        display: "flex",
        flexDirection: "column",
        gap: "30px",
        alignItems: "flex-start",
        marginRight: "40px",
      }}
    >
      {listArr.map((singleList) => {
        // console.log(singleList);
        return (
          <li key={singleList} style={{listStyle:"none"}}>
            <input
              type="checkbox"
              // id={singleList}
              value={singleList}
              onChange={HandleCheckBox}
              // checked={false}
            />
            <label for="item2">{singleList}</label>
          </li>
        );
      })}
    </ul>
  );
}

export default List;
