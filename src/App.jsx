import { useState } from "react";
import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import List from "./component/List";
import "./App.css";

function App() {
  // const [count, setCount] = useState(0)
  const [left, setLeft] = useState(["js", "Hmtl", "CSS", "ts"]);
  const [right, setRight] = useState(["react", "angular", "vue", "svelte"]);
  const [tempLeft, setTempLeft] = useState([]);
  const [tempRight, setTempRight] = useState([]);
  // left list fucntion
  function handlerLeftList(transferList) {
    console.log(transferList);
    setTempLeft(transferList);
  }

  function leftShiftHandler() {
    if (left.length == 0) {
      alert("no element in left list");
    }
    const tempArr = right;
    const tempArrLeft = [];
    tempLeft.forEach((ele) => {
      tempArr.push(ele);
    });

    left.forEach((ele) => {
      if (!tempLeft.includes(ele)) {
        tempArrLeft.push(ele);
      }
    });
    setTempLeft([]);
    setRight(tempArr);
    setLeft(tempArrLeft);
  }
  function allLeftHandler() {
    const tempArr = [...left, ...right];

    setLeft(tempArr);
    setRight([]);
  }

  // right list fucntions

  function handlerRightList(transferList) {
    console.log(transferList);
    setTempRight(transferList);
  }
  function rightShiftHandler() {
    if (right.length == 0) {
      alert("no element in right list");
    }
    // console.log(tempLeft);
    const tempArr = left;
    const tempArrRight = [];
    tempRight.forEach((ele) => {
      tempArr.push(ele);
      console.log(tempArr);
    });

    right.forEach((ele) => {
      if (!tempRight.includes(ele)) {
        tempArrRight.push(ele);
      }
    });
    setTempRight([]);
    setLeft(tempArr);
    setRight(tempArrRight);
  }
  function allRightHandler() {
    const tempArr = [...left, ...right];

    setLeft([]);
    setRight(tempArr);
  }

  return (
    <div className="lists-container" style={{ display: "flex" }}>
      <List listArr={left} onListChange={handlerLeftList} />
      <div
        className="buttont-container"
        style={{ display: "flex", flexDirection: "column", gap: "20px" }}
      >
        <button className="all-left" onClick={allLeftHandler}>
          All right to left
        </button>
        <button className="left" onClick={leftShiftHandler}>
          Left to right
        </button>
        <button className="righ" onClick={rightShiftHandler}>
          rigth to left
        </button>
        <button className="all-right" onClick={allRightHandler}>
          All left to right
        </button>
      </div>
      <List listArr={right} onListChange={handlerRightList} />
    </div>
  );
}

export default App;
